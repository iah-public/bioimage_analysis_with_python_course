<!---

BSD License

Copyright (c) 2022, Kevin Yamauchi
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice, this
  list of conditions and the following disclaimer in the documentation and/or
  other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from this
  software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
OF THE POSSIBILITY OF SUCH DAMAGE.

-->

# Setting up your python environment

## Installing conda to manage your python environment

In this tutorial, we will use `conda` to manage our python environment. If you haven't installed conda yet, follow the instructions [here](install_python.md).


## Setting up your environment

```{admonition} Using conda together with mamba?
There is one problem with older versions of conda: It is very slow. Alternatively, you can use `mamba`, a drop-in replacement for conda that is much faster. If you have installed `mamba` into your conda environment (`conda install -c conda-forge mamba`), you can replace `conda` with `mamba` in the following instructions.

```

1. Open your terminal.
	- **Windows**: Open the "miniforge prompt" from your start menu
	- **Mac OS**: Open Terminal (you can search for it in spotlight - cmd + space)
	- **Linux**: Open your terminal application


2. Navigate to the `notebooks` subdirectory of the `bioimage_analysis_with_python_course` directory you downloaded.

	```bash
	cd <path to bioimage_analysis_with_python_course>/notebooks
	```


3. The file `environment.yml` contains the dependencies needed to run the notebooks, and it specifies a `conda` environment named `pyimagecourse`. Create this environment from the file by entering the following command.

	```bash
	conda env create --file environment.yml
	```

4. Once the environment setup has finished, activate the environment. If you successfully activated the environment, you should now see `(pyimagecourse)` to the left of your command prompt.

	```bash
	conda activate pyimagecourse
	```


5. Test that your notebook installation is working. We will be using notebook for interactive analysis. Enter the command below and it should launch jupyter notebook book in a web browser.

	```bash
	jupyter notebook
	```

6. Test your napari installation. Enter the command below and an empty napari viewer should open. You can close the window after it opens. Please note that it takes a bit of extra time to launch napari the first time.
	
	```bash
	napari
	```
