# Launching the notebooks

## Launch jupyter notebook

Open your terminal and navigate to the `notebooks` subdirectory of the `bioimage_analysis_with_python_course` directory you downloaded.

```
cd <path to bioimage_analysis_with_python_course>/notebooks
```

Now activate your `pyimagecourse` conda environment you created in the installation step.

```
conda activate pyimagecourse
```

To start the Jupyter Notebook server, enter

```bash
jupyter-lab
```

Jupyter Notebook will open in a browser window and you will see the course notebooks.


### Alternative: Run the notebooks in the cloud

The notebooks in this repository can be run interactively using different cloud services. You can use the badges below to run the notebooks either
- in the classical Jupyter environment via
  - MyBinder
  - RenkuLab (with graphical support to run `napari`)
- as Google Colab notebooks

The Mybinder and Renkulab sessions are only temporary, i.e. changes you make to notebooks or new notebooks are *erased* between sessions. When using Colab, to keep your changes, you need to *save a copy* of the notebook you are modifying. The saving is done in your Google Drive.

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/m-albert/PyImageCourse_beginner/master?urlpath=lab)
[![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/github/m-albert/PyImageCourse_beginner/blob/master)
[![launch - renku](https://img.shields.io/badge/launch-renku-2ea44f?logo=python)](https://renkulab.io/projects/marvin.albert/pyimagecourse-beginner/sessions/new?autostart=1)