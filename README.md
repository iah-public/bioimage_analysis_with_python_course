[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/m-albert/PyImageCourse_beginner/master?urlpath=lab)
[![Open In Colab](https://colab.research.google.com/assets/colab-badge.svg)](https://colab.research.google.com/github/m-albert/PyImageCourse_beginner/blob/master)
[![launch - renku](https://img.shields.io/badge/launch-renku-2ea44f?logo=python)](https://renkulab.io/projects/marvin.albert/pyimagecourse-beginner/sessions/new?autostart=1)
[![DOI](https://zenodo.org/badge/173477371.svg)](https://zenodo.org/badge/latestdoi/173477371)


# Overview

Course [website](https://iah-public.pages.pasteur.fr/bioimage_analysis_with_python_course) and [slides](https://docs.google.com/presentation/d/1u_FraZPABjCdzNj_2Oq5i7FxWbGPdIkbtCKXzxAORwA/edit?usp=sharing).

## Schedule

- Presentation
  - Course intro
  - How to work with python and jupyter notebooks
  - Python installation and environments
- Hands-on:
  - Installation
  - Python essentials + exercise
- Presentation:
  - Numpy and image processing in Python
- Hands-on work with notebooks:
  - Guided walk-through
  - Exercises


## This repository

This repository contains a set of Jupyter notebooks to learn how to do basic image processing using Python and the scientific packages Numpy, scikit-image, Matplotlib and Pandas.

The material assumes no pre-existing knowledge in programming but some familiarity with concepts of image processing. The goal of the course is not to learn all the details of Python, but to reach as fast as possible the ability to write short image processing scripts. The course therefore focuses only on essential knowledge and is not at all *exhaustive*.

## Installation instructions for practical

Follow the instructions provided [here](https://iah-public.pages.pasteur.fr/bioimage_analysis_with_python_course/README.html) to [download](book/download_repo.md) the course material, [set up](book/setup_environment.md) a python environment and [launch](book/launch_notebooks.md) the notebooks provided in this repository.

### Alternative: Run the notebooks in the cloud

The notebooks in this repository can be run interactively using different cloud services. You can use the badges at the top of this README to run the notebooks either
- in the classical Jupyter environment via
  - MyBinder
  - Renkulab (with graphical support to run `napari`)
- as Google Colab notebooks

The Mybinder and Renkulab sessions are only temporary, i.e. changes you make to notebooks or new notebooks are *erased* between sessions. When using Colab, to keep your changes, you need to *save a copy* of the notebook you are modifying. The saving is done in your Google Drive.

## Acknowledgements / Copyright

The material for this course have been modified from
[this](https://github.com/guiwitz/PyImageCourse_beginner) [![DOI](https://zenodo.org/badge/173477371.svg)](https://zenodo.org/badge/latestdoi/173477371)  (notebooks) and
[this](https://github.com/guiwitz/neubias_academy_biapy) [![DOI](https://zenodo.org/badge/261601095.svg)](https://zenodo.org/badge/latestdoi/261601095) (slides) course, both created by [Guillaume Witz](https://twitter.com/guiwitz). Thank you for making this teaching material available to the community.

Installation instructions have been modified from the [Open Image Data Handbook](https://kevinyamauchi.github.io/open-image-data/intro.html) by [Kevin Yamauchi](https://kevinyamauchi.github.io/). Thank you.


## Data

In this course, we are trying to reproduce a workflow used in other contexts, in particular Fiji, so that you can compare different approaches. For example you can check the excellent introduction to Fiji Macro programming by Anna Klemm [here](https://github.com/ahklemm/ImageJMacro_Introduction). We use images from the [Cell Atlas](https://www.proteinatlas.org/humanproteome/cell) of the Human Protein Atlas (HPA) project where a large collection of proteins have been tagged and imaged to determine their cellular location. Specifically, we downloaded a series of [images](images) from the Atlas, with some cells showing nucleoplasm localization and some nuclear membrane localization. The idea of the workflow is to compare the signal within the nucleus with that on its edge to determine for each image whether the protein is membrane bound or not.

The images in the [images](images) folder all come from the [Cell Atlas](https://www.proteinatlas.org/humanproteome/cell). For more information see the publication of Thul PJ et al., A subcellular map of the human proteome. **Science**. (2017) DOI: [10.1126/science.aal3321](https://doi.org/10.1126/science.aal3321). All images are covered by a [Creative Commons Attribution-ShareAlike 3.0 International License](https://creativecommons.org/licenses/by-sa/3.0/).

All images were downloaded directly from the HPA website using the same link construction and saved as tif files. For example the image [8346_22_C1_1.tif](images/8346_22_C1_1.tif) was downloaded using the link
https://images.proteinatlas.org/8346/22_C1_1_blue_red_green.jpg.

